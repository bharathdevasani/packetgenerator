#include "getvalues.h"
#include "arpa/inet.h"
#include "string.h"
#include <string>
#include <bitset>
using namespace std;

bool GetValues::isEmpty(QLineEdit *qline){
    if (qline->text().size() > 0)
        return false;
    else
        return true;
}

bool GetValues::checkIP(QLineEdit *qline){
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, qline->text().toUtf8().constData(), &(sa.sin_addr));
    return result != 0;
}

bool GetValues::checkValue(QLineEdit *qline){
    if(qline->text().toInt() < 0)
        return false;
    else
        return true;
}

int GetValues::getValuesofQSpinbox(QSpinBox *qsbox){
    return qsbox->text().toInt();
}

int GetValues::getIndexOfComboBox(QComboBox *qbox){
    return qbox->currentIndex();
}

QString GetValues::getTextofQComboBox(QComboBox *qbox){
    return qbox->currentText();
}

QString GetValues::getValuesofQLineEdit(QLineEdit *qline){
    return qline->text();
}

int GetValues::getTos(string dscp0_2, string dscp3_5, string ecn6_7){
    string tos = dscp0_2 + dscp3_5 + ecn6_7;
    return stoi(tos, nullptr, 2);
}

