#ifndef GETVALUES_H
#define GETVALUES_H
#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>
#include <string>
using namespace std;

class GetValues
{
public:
    bool isEmpty(QLineEdit *qline);
    bool checkIP(QLineEdit *qline);
    bool checkValue(QLineEdit *qline);
    int getIndexOfComboBox(QComboBox *qbox);
    int getValuesofQSpinbox(QSpinBox *qsbox);
    QString getTextofQComboBox(QComboBox *qline);
    QString getValuesofQLineEdit(QLineEdit *qline);
    int getTos(string dscp0_2, string dscp3_5, string ecn6_7);
};

#endif // GETVALUES_H
