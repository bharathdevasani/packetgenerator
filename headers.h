#ifndef HEADERS_H
#define HEADERS_H
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

class Headers
{
public:
    unsigned short in_cksum(unsigned short *addr, int len);
};

#endif // HEADERS_H
