#include "ipheader.h"

unsigned char Ipheader::getIpVersion() const
{
    return IPH.ipVersion;
}

void Ipheader::setIpVersion(unsigned char value)
{
    IPH.ipVersion = value;
}

unsigned char Ipheader::getIpLength() const
{
    return IPH.ipLength;
}

void Ipheader::setIpLength(unsigned char value)
{
    IPH.ipLength = value;
}

unsigned char Ipheader::getTos() const
{
    return IPH.tos;
}

void Ipheader::setTos(unsigned char value)
{
    IPH.tos = value;
}

unsigned short Ipheader::getLength() const
{
    return IPH.length;
}

void Ipheader::setLength(unsigned short value)
{
    IPH.length = value;
}

unsigned short Ipheader::getIdentification() const
{
    return IPH.identification;
}

void Ipheader::setIdentification(unsigned short value)
{
    IPH.identification = value;
}

unsigned short Ipheader::getFragOffset() const
{
    return IPH.FragOffset;
}

void Ipheader::setFragOffset(unsigned short value)
{
    IPH.FragOffset = value;
}

unsigned char Ipheader::getTtl() const
{
    return IPH.ttl;
}

void Ipheader::setTtl(unsigned char value)
{
    IPH.ttl = value;
}

unsigned char Ipheader::getProtocol() const
{
    return IPH.protocol;
}

void Ipheader::setProtocol(unsigned char value)
{
    IPH.protocol = value;
}

unsigned short Ipheader::getChecksum() const
{
    return IPH.checksum;
}

void Ipheader::setChecksum(unsigned short value)
{
    IPH.checksum = value;
}

unsigned int Ipheader::getSource_address() const
{
    return IPH.source_address;
}

void Ipheader::setSource_address(unsigned int value)
{
    IPH.source_address = value;
}

unsigned int Ipheader::getDestination_address() const
{
    return IPH.destination_address;
}

void Ipheader::setDestination_address(unsigned int value)
{
    IPH.destination_address = value;
}

unsigned char Ipheader::getR0() const
{
    return IPH.r0;
}

void Ipheader::setR0(unsigned char value)
{
    IPH.r0 = value;
}

unsigned char Ipheader::getDf() const
{
    return IPH.df;
}

void Ipheader::setDf(unsigned char value)
{
    IPH.df = value;
}

unsigned char Ipheader::getMf() const
{
    return IPH.mf;
}

void Ipheader::setMf(unsigned char value)
{
    IPH.mf = value;
}

unsigned short Ipheader::csum(unsigned short *buf, int nwords)
{       //
        unsigned long sum;
        for(sum=0; nwords>0; nwords--)
                sum += *buf++;
        sum = (sum >> 16) + (sum &0xffff);
        sum += (sum >> 16);
        return (unsigned short)(~sum);
}

Ipheader::Ipheader()
{

}
