#ifndef IPHEADER_H
#define IPHEADER_H

class Ipheader
{

private:

public:
    struct IPHEADER
      {
        unsigned char ipVersion:4;
        unsigned char ipLength:4;
        unsigned char tos;
        unsigned short int length;
        unsigned short int identification;
        unsigned char r0:1;
        unsigned char df:1;
        unsigned char mf:1;
        unsigned short int FragOffset;
        unsigned char ttl;
        unsigned char protocol;
        unsigned short int checksum;
        unsigned int source_address;
        unsigned int destination_address;

    };
    struct IPHEADER IPH;
    Ipheader();
    unsigned char getIpVersion() const;
    void setIpVersion(unsigned char value);
    unsigned char getIpLength() const;
    void setIpLength(unsigned char value);
    unsigned char getTos() const;
    void setTos(unsigned char value);
    unsigned short getLength() const;
    void setLength(unsigned short value);
    unsigned short getIdentification() const;
    void setIdentification(unsigned short value);
    unsigned short getFragOffset() const;
    void setFragOffset(unsigned short value);
    unsigned char getTtl() const;
    void setTtl(unsigned char value);
    unsigned char getProtocol() const;
    void setProtocol(unsigned char value);
    unsigned short getChecksum() const;
    void setChecksum(unsigned short value);
    unsigned int getSource_address() const;
    void setSource_address(unsigned int value);
    unsigned int getDestination_address() const;
    void setDestination_address(unsigned int value);
    unsigned char getR0() const;
    void setR0(unsigned char value);
    unsigned char getDf() const;
    void setDf(unsigned char value);
    unsigned char getMf() const;
    void setMf(unsigned char value);
    unsigned short csum(unsigned short *buf, int nwords);
};

#endif // IPHEADER_H
