#include "logger.h"
#include "string.h"
const static int MAX_LOG_LENGTH = 2048;

void Logger::log(ELevel level,QTextBrowser *LogBrowser ,const QString& log_info) {
    time_t time_now = time(NULL);
    struct tm tm_now = *localtime(&time_now); //multithread problem

    char log_head[MAX_LOG_LENGTH];
    snprintf(log_head, sizeof(log_head), "%04d-%02d-%02d %02d:%02d:%02d", tm_now.tm_year + 1900,
            tm_now.tm_mon + 1, tm_now.tm_mday, tm_now.tm_hour, tm_now.tm_min,
            tm_now.tm_sec);

    size_t tmp_buf_len = strlen(log_head);
    QString wrapper = QString("%1%2");
    switch (level) {
    case TRACE:
        wrapper = QString("<font color=green>%1%2</font>");
        strncat(log_head, " [TRACE] ", sizeof(log_head) - tmp_buf_len);
        break;
    case DEBUG:
        wrapper = QString("<font color=blue>%1%2</font>");
        strncat(log_head, " [DEBUG] ", sizeof(log_head) - tmp_buf_len);
        break;
    case INFO:
        strncat(log_head, " [INFO] ", sizeof(log_head) - tmp_buf_len);
        break;
    case WARN:
        strncat(log_head, " [WARN] ", sizeof(log_head) - tmp_buf_len);
        break;
    case ERROR1:
        strncat(log_head, " [ERROR] ", sizeof(log_head) - tmp_buf_len);
        wrapper = QString("<font color=red>%1%2</font>");
        break;
    default:
        break;
    }
    log_head[sizeof(log_head) - 1] = '\0';
    tmp_buf_len = strlen(log_head);
    //snprintf(log_head + tmp_buf_len, sizeof(log_head) - tmp_buf_len, "%s(%d) - ", "Main", 12);
    log_head[sizeof(log_head) - 1] = '\0';

    if (1==1) {
        LogBrowser->append(wrapper.arg(log_head).arg(log_info));
    }
}
