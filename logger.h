#ifndef LOGGER_H
#define LOGGER_H
#include <stdio.h>
#include <string.h>
#include <QString>
#include <QObject>
#include <QTextBrowser>
class Logger
{
public:
    enum ELevel {
            TRACE = 1, DEBUG, INFO, WARN, ERROR1
        };
    void log(ELevel level, QTextBrowser *LogBrowser  , const QString& log_info);
    //void log(QTextBrowser *LogBrowser);
};

#endif // LOGGER_H
