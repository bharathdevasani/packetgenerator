#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logger.h"
#include "getvalues.h"
#include <string>
#include <stdint.h>
#define _BSD_SOURCE
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include "headers.h"
#include "staticvalues.h"
#include "ipheader.h"
#include "udpheader.h"

#define PACK_LEN 8192


using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /* Initializing ttl values
     * Range 1 - 255
     */

    for (int i=1; i < 256; i++ ){
        ui->ttl->addItem(QString::number(i));
    }

    // IP version initializer

   for (int i=0; i < 15; i++){
        ui->ipversion->addItem(QString::number(i));
   }

   // Internet Header Length

   for (int i=0; i < 15; i++){
        ui->internetHeaderLength->addItem(QString::number(i));
   }

    /*
     * Regex for IP Address
     */
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonBox_accepted()
{
    Logger logger;
    GetValues Validator;
    Headers hdrs;
    Ipheader *IP;

    /* --------------------------------------------------
     *
     *          @Initializers
     *
     * --------------------------------------------------*/
    char buffer[PACK_LEN];
    int iError = 0;
    int dscbits_0_2 = 0;
    int dscbits_3_6 =0;
    int ecnbits_6_7 = 0;
    int packetlength  = 0;
    int Flag_DF = 0;
    int Flag_MF = 0;
    int offset = 0;
    int ttl = 4;
    int packetsToSend = 1;
    int packetInterval = 10;
    int ip_version = 4;
    int internet_Header_Length = 0;

    /*
     *
     *  Headers
     *
     *
     */

    // Get IP Version

    ip_version = Validator.getIndexOfComboBox(ui->ipversion);

    IP->setIpVersion(ip_version);

    // Get Internet Header Length

    internet_Header_Length = Validator.getIndexOfComboBox(ui->internetHeaderLength);

    IP->setIpLength(internet_Header_Length);

    // Validating Source IP

    if(Validator.isEmpty(ui->sourceip)){
       logger.log(logger.ERROR1,ui->LoggerBrowser,"SOURCE IP NOT ENTERED");
       iError = 1;
       IP->setSource_address(inet_addr("127.0.0.1"));
    }else{
        if(!Validator.checkIP(ui->sourceip)){
           logger.log(logger.ERROR1,ui->LoggerBrowser,"SOURCE IP PROVIDED is INCORRECT");
           iError = 1;
           IP->setSource_address(inet_addr("127.0.0.1"));
        }else{
           IP->setSource_address(inet_addr("127.0.0.1"));
        }
    }

    // Validating Destination IP
    if(Validator.isEmpty(ui->destinationIP)){
       logger.log(logger.ERROR1,ui->LoggerBrowser,"DESTINATION IP NOT ENTERED");
        iError = 1;
        IP->setDestination_address(inet_addr("1.1.1.1"));
    }else{
        if(!Validator.checkIP(ui->destinationIP)){
           logger.log(logger.ERROR1,ui->LoggerBrowser,"DESTINATION IP PROVIDED is INCORRECT");
           iError = 1;
           IP->setDestination_address(inet_addr("1.1.1.1"));
        }else{
           IP->setDestination_address(inet_addr("1.1.1.1"));
        }
    }

    // get Values of DS-ECN BITS
    dscbits_0_2 = Validator.getIndexOfComboBox(ui->dscpbits02);

    dscbits_3_6 = Validator.getIndexOfComboBox(ui->dscpbits35);
    ecnbits_6_7 = Validator.getIndexOfComboBox(ui->ecnbits67);

    // Packet Override
    if(ui->checkOverridepacketLength->isChecked()){
        if(ui->packetLength->text()> 0 and !Validator.checkValue(ui->packetLength)){
            QString pl = Validator.getValuesofQLineEdit(ui->packetLength);
            logger.log(logger.DEBUG,ui->LoggerBrowser, "Packet Length is " + pl);
            packetlength = pl.toInt();
            IP->setLength(packetlength);
        }else{
            iError = 1;
            logger.log(logger.ERROR1,ui->LoggerBrowser,"Packet length is NOT ENTERED");
            //IP.setLength(42);
        }
    }else{
        logger.log(logger.DEBUG,ui->LoggerBrowser,"OverRide Packet Length Not Checked");
        IP->setLength(42);
    }

    // Identification Field Value
    if(Validator.isEmpty(ui->IdentificationFieldValue) and Validator.checkValue(ui->IdentificationFieldValue) ){
        iError = 1;
        logger.log(logger.ERROR1, ui->LoggerBrowser, "Identification Field Is Not ENTERED");
        IP->setIdentification(321);
    }else{
        QString il = Validator.getValuesofQLineEdit(ui->IdentificationFieldValue);
        logger.log(logger.DEBUG,ui->LoggerBrowser,"Identification Value is " + il);
        IP->setIdentification(il.toInt());
    }

    // Packet Type

    QString packetType = Validator.getTextofQComboBox(ui->packetType);
    logger.log(logger.DEBUG,ui->LoggerBrowser,"Packet Type " + packetType);
    IP->setProtocol(17);

    // Flags ( DF and MF Flag)

    IP->setR0(0);
    Flag_DF = Validator.getIndexOfComboBox(ui->dfFlag);
    IP->setDf(Flag_DF);
    Flag_MF = Validator.getIndexOfComboBox(ui->mfFlag);
    IP->setMf(Flag_MF);
    // Offset
    offset = Validator.getValuesofQLineEdit(ui->offset).toInt();
    if (offset < 0){
        iError =1;
        logger.log(logger.ERROR1, ui->LoggerBrowser, "ENTER PROPER OFFSET");
        IP->setFragOffset(0);
    }else{
        IP->setFragOffset(offset);
    }

    // TTL

    if (Validator.getTextofQComboBox(ui->ttl).toInt() <= 0){
        iError = 1;
        logger.log(logger.ERROR1, ui->LoggerBrowser, "ENTER PROPER TTL VALUE");
        IP->setTtl(1);
    }else{
        ttl = Validator.getTextofQComboBox(ui->ttl).toInt();
        logger.log(logger.DEBUG, ui->LoggerBrowser, "TTL Value is " + QString::number(ttl));
        IP->setTtl(ttl);
    }

    // Number to send
    if(Validator.getValuesofQSpinbox(ui->numberToSend) <= 0){
        iError = 1;
        logger.log(logger.DEBUG, ui->LoggerBrowser, "DEFAULT NUMBER OF PACKETS TO SEND IS 1");
    }else{
        packetsToSend = Validator.getValuesofQSpinbox(ui->numberToSend);
        logger.log(logger.DEBUG,ui->LoggerBrowser, "SEND PACKETS (NO) " + QString::number(packetsToSend));
    }

    // Packet Interval

    if (Validator.getValuesofQSpinbox(ui->packetInterval) <=0){
        iError = 1;
        logger.log(logger.ERROR1, ui->LoggerBrowser, "Packet Interval CANNOT BE LESS THAN 0");
    }else{
        packetInterval = Validator.getValuesofQSpinbox(ui->packetInterval);
        logger.log(logger.DEBUG,ui->LoggerBrowser, "Packet IntervaL selected (NO) " + QString::number(packetInterval) + " (ms)");
    }



    if (packetType == "UDP"){
        UDPheader *udph;
        IP->setLength(sizeof(IP->IPH)+sizeof(udph->UDP));
        IP->setChecksum(IP->csum((unsigned short *)buffer, sizeof(IP->IPH) + sizeof(udph->UDP)));
        udph->setUdph_srcport(htons(8888));
        udph->setUdph_destport(htons(5656));
        udph->setUdph_len(sizeof(udph->UDP));
        udph->setUdph_chksum(udph->csum((unsigned short *)buffer, sizeof(udph->UDP)));
    }


    // Check for any error and send if no error found.
    if(!iError){
        if(ui->sendContinuously->isChecked()){
            // Send Continuously


        }else{

        }
    }

}

void MainWindow::on_buttonBox_rejected()
{
    QApplication::quit();
}
