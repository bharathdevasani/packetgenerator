#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QString>
#include <QLineEdit>
#include <QRegExp>
#include <QRegExpValidator>
#include <QMessageBox>
#include <iostream>
#include <string>

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    //void Logger();


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
