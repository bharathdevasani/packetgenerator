#include "udpheader.h"


unsigned short UDPheader::getUdph_srcport() const
{
    return UDP.udph_srcport;
}

void UDPheader::setUdph_srcport(unsigned short value)
{
    UDP.udph_srcport = value;
}

unsigned short UDPheader::getUdph_destport() const
{
    return UDP.udph_destport;
}

void UDPheader::setUdph_destport(unsigned short value)
{
    UDP.udph_destport = value;
}

unsigned short UDPheader::getUdph_len() const
{
    return UDP.udph_len;
}

void UDPheader::setUdph_len(unsigned short value)
{
    UDP.udph_len = value;
}

unsigned short UDPheader::getUdph_chksum() const
{
    return UDP.udph_chksum;
}

void UDPheader::setUdph_chksum(unsigned short value)
{
    UDP.udph_chksum = value;
}

int UDPheader::getSizeof()
{
    return sizeof(UDP);
}

unsigned short UDPheader::csum(unsigned short *buf, int nwords)
{
    unsigned long sum;
    for(sum=0; nwords>0; nwords--)
            sum += *buf++;
    sum = (sum >> 16) + (sum &0xffff);
    sum += (sum >> 16);
    return (unsigned short)(~sum);
}



