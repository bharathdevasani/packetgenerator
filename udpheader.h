#ifndef UDP_H
#define UDP_H


class UDPheader
{


public:
    struct udp {
        unsigned short int udph_srcport;
        unsigned short int udph_destport;
        unsigned short int udph_len;
        unsigned short int udph_chksum;
    }; /* total udp header length: 8 bytes (= 64 bits) */
    struct udp UDP;

    unsigned short getUdph_srcport() const;
    void setUdph_srcport(unsigned short value);
    unsigned short getUdph_destport() const;
    void setUdph_destport(unsigned short value);
    unsigned short getUdph_len() const;
    void setUdph_len(unsigned short value);
    unsigned short getUdph_chksum() const;
    void setUdph_chksum(unsigned short value);
    int getSizeof();
    unsigned short csum(unsigned short *buf, int nwords);
};

#endif // UDP_H
